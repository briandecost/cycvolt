from distutils.core import setup

setup(
    name='cycvolt',
    version='0.0.1',
    description='analysis tools for cyclic voltammetry data',
    packages=['cycvolt',],
    license='NIST public domain license',
    long_description=open('README.md').read(),
)
